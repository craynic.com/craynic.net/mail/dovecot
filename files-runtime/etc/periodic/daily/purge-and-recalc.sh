#!/usr/bin/env bash

# recalc quotas first
doveadm quota recalc -A

# purge mailboxes
doveadm purge -A

# recalc quotas again
doveadm quota recalc -A
