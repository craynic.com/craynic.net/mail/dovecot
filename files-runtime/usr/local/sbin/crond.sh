#!/usr/bin/env bash

set -Eeuo pipefail

LOGLEVEL="6"

/usr/sbin/crond -f -d "$LOGLEVEL" 2>&1 | ts
