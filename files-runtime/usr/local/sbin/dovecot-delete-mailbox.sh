#!/usr/bin/env bash

set -Eeuo pipefail

export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

USERNAME="$1"
DOMAIN="${USERNAME#*@}"
MAILBOX="${USERNAME%%@*}"

# https://dovecot.org/list/dovecot/2015-February/099655.html

# remove all e-mails
doveadm expunge -d -u "$USERNAME" mailbox '*' ALL
# purge (and remove attachments if not used anywhere else)
doveadm -v purge -u "$USERNAME"
# remove the mailbox folder
rm -rf -- "/var/mail/$DOMAIN/$MAILBOX"
