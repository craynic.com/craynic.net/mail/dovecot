#!/usr/bin/env bash

set -Eeuo pipefail

export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

USERNAME="$1"

for MAILBOX in INBOX Drafts Junk Sent Trash; do
  doveadm mailbox create -u "$USERNAME" -s "$MAILBOX" 2>/dev/null
done
