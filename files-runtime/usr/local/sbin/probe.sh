#!/usr/bin/env bash

set -Eeuo pipefail

/usr/local/sbin/healthcheck.sh 24 110 143 465 587 993 995 4190 10110 10143 10465 10587 10993 10995 14190
