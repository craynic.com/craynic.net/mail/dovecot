#!/usr/bin/env bash

set -Eeuo pipefail

MYSQL_CMD=(mysql)
MYSQL_TABLE_NAME="dovecot_junk_settings"
MYSQL_CMD+=(-N)
DEFAULT_JUNK_FOLDER_NAME="Junk"

# Thanks to CONCAT, we can differentiate between no row and empty value:
# empty output means no row; while ";" means no value
QUERY="SELECT CONCAT(\`junk_folder_name\`, \";\") FROM \`$MYSQL_TABLE_NAME\` WHERE \`username\`=\"$USER\" LIMIT 1";
OUT="$(echo "$QUERY" | "${MYSQL_CMD[@]}")"

# empty output = no rows, we use the default
if [[ "$OUT" = "" ]]; then
  echo -n "$DEFAULT_JUNK_FOLDER_NAME"
else
  echo -n "${OUT::-1}"
fi
