#!/usr/bin/env bash

set -Eeuo pipefail

RSPAM_HOST=""
RSPAM_PASSWORD=""

if [[ -z "$RSPAM_HOST" ]]; then
  exit 0
fi

/usr/bin/rspamc -h "$RSPAM_HOST" -P "$RSPAM_PASSWORD" learn_spam <&0
