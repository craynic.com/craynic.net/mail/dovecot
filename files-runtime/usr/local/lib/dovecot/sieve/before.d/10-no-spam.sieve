require ["fileinto", "imap4flags", "vnd.dovecot.execute", "variables"];

if header :is "X-Spam" "Yes" {
    if execute :output "junk_folder_name" "get-junk-folder-name.sh" {
        if not string :is "${junk_folder_name}" "" {
            fileinto "${junk_folder_name}";
            stop;
        }
    }
}
