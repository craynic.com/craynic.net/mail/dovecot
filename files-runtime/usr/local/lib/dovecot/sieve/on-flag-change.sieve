require ["vnd.dovecot.pipe", "copy", "imapsieve", "imap4flags"];

if allof(hasflag :is ["Junk"], hasflag :is ["NonJunk"]) {
} elsif hasflag :is ["Junk"] {
  pipe :copy "sieve-learn-spam";
} elsif hasflag :is ["NonJunk"] {
  pipe :copy "sieve-learn-ham";
}
