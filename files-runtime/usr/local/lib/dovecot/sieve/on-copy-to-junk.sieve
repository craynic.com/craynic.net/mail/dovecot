require ["vnd.dovecot.pipe", "copy", "imapsieve", "imap4flags"];

if anyof(hasflag :is ["Junk"], hasflag :is ["NonJunk"]) {
  stop;
}

pipe :copy "sieve-learn-spam";
