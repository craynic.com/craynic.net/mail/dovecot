#!/usr/bin/env bash

set -Eeuo pipefail

CFG_FILES=(/etc/dovecot*/dovecot.conf)

if [[ -z "$DOVECOT_SUBMISSION_MAX_MAIL_SIZE" ]]; then
  exit 0
fi

sed -i "/^#\?submission_max_mail_size[[:space:]]*=/c\\submission_max_mail_size = $DOVECOT_SUBMISSION_MAX_MAIL_SIZE" \
  -- "${CFG_FILES[@]}"
