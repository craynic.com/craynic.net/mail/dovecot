#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$DOVECOT_AUTH_VERBOSE" && -z "$DOVECOT_AUTH_DEBUG" ]]; then
  exit 0
fi

CFG_FILES=(/etc/dovecot*/dovecot.conf)

echo "" | tee -a "${CFG_FILES[@]}" >/dev/null

if [[ "$DOVECOT_AUTH_VERBOSE" =~ ^(yes|true|1|on)$ ]]; then
  echo "Enabling verbose authentication logging"
  echo "auth_verbose = yes" | tee -a "${CFG_FILES[@]}" >/dev/null
fi

if [[ "$DOVECOT_AUTH_DEBUG" =~ ^(yes|true|1|on)$ ]]; then
  echo "Enabling authentication debug logging"
  echo "auth_debug = yes" | tee -a "${CFG_FILES[@]}" >/dev/null
fi
