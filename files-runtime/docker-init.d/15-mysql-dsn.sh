#!/usr/bin/env bash

set -Eeuo pipefail

CFG_FILES=(/etc/dovecot*/dovecot*sql.conf.ext)

if [[ -z "$DOVECOT_MYSQL_DSN" ]]; then
  exit 0
fi

echo "MySQL DSN set, configuring MySQL & enabling SQL-related functions"

PATTERN="^\([^:]\+\):\/\/\([^:]\+\):\([^@]\+\)@\([^:/]\+\)\(:\([^/]\+\)\)\?\/\([^?]\+\).*$"

# 1. /etc/dovecot/dovecot-quota-sql.conf
REPLACE="connect = host=\4 port=\6 user=\2 password=\3 dbname=\7"
CONNECT_STRING="$(echo -n "$DOVECOT_MYSQL_DSN" | sed "s/$PATTERN/$REPLACE/" | sed "s/port= //")"

if [[ -n "$DOVECOT_MYSQL_CA" ]]; then
  CONNECT_STRING="${CONNECT_STRING} ssl_ca=${DOVECOT_MYSQL_CA}"
fi

sed -i "/^#\?connect *=/c\\$CONNECT_STRING" -- "${CFG_FILES[@]}"

mv /etc/dovecot/dovecot-quota-sql.conf.disabled /etc/dovecot/dovecot-quota-sql.conf

# 2. /usr/local/lib/dovecot/sieve-extprograms/get-junk-folder-name.sh
GET_JUNK_FOLDER_NAME_SCRIPT_FILENAME="/usr/local/lib/dovecot/sieve-extprograms/get-junk-folder-name.sh"
REPLACE="(mysql -h\"\4\" -P\"\6\" -D\"\7\" -u\"\2\" -p\"\3\")"
MYSQL_CMD="$(echo -n "$DOVECOT_MYSQL_DSN" | sed "s/$PATTERN/$REPLACE/" | sed "s/ -P\"\"//")"

if [[ -n "$DOVECOT_MYSQL_CA" ]]; then
  MYSQL_CMD="${MYSQL_CMD::-1} --ssl-ca=\"$DOVECOT_MYSQL_CA\")"
fi

sed -i "/^MYSQL_CMD *=/c\\MYSQL_CMD=$MYSQL_CMD" -- "$GET_JUNK_FOLDER_NAME_SCRIPT_FILENAME"
