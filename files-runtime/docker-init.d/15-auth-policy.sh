#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$DOVECOT_AUTH_POLICY_ENDPOINT" || -z "$DOVECOT_AUTH_POLICY_HASH_NONCE" ]]; then
  exit 0
fi

TEMPLATE_FILES=(/etc/dovecot*/auth-policy.conf.template)

for TEMPLATE_FILE in "${TEMPLATE_FILES[@]}"; do
  CFG_FILE="${TEMPLATE_FILE%.template}"
  # shellcheck disable=SC2016
  envsubst '${DOVECOT_AUTH_POLICY_ENDPOINT} ${DOVECOT_AUTH_POLICY_HASH_NONCE}' < "$TEMPLATE_FILE" > "$CFG_FILE"

  echo "" >> "$CFG_FILE"
done
