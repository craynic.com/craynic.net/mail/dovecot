#!/usr/bin/env bash

set -Eeuo pipefail

CFG_FILES=(/etc/dovecot*/dovecot.conf)

if [[ "$DOVECOT_SSL_CLIENT_REQUIRE_VALID_CERT" =~ ^(no|false|0|off)$ ]]; then
  echo "!!! Allowing invalid TLS certificates for SSL client !!!"

  echo "ssl_client_require_valid_cert = no" | tee -a "${CFG_FILES[@]}" >/dev/null
fi
