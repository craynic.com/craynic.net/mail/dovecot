#!/usr/bin/env bash

set -Eeuo pipefail

MAIL_FOLDER="/var/mail/"

if ! [[ "$DOVECOT_STORAGE_FIX_PERMISSIONS_ON_STARTUP" =~ ^(yes|true|1|on)$ ]]; then
  exit 0
fi

if [[ "$DOVECOT_STORAGE_FIX_PERMISSIONS_ON_STARTUP_RECURSIVE" =~ ^(yes|true|1|on)$ ]]; then
  echo "Fixing permissions for $MAIL_FOLDER recursively."

  chown -R mail:mail -- "$MAIL_FOLDER"
  chmod -R u+rwX,go-rwx -- "$MAIL_FOLDER"
else
  echo "Fixing permissions for $MAIL_FOLDER."

  chown mail:mail -- "$MAIL_FOLDER"
  chmod u+rwX,go-rwx -- "$MAIL_FOLDER"
fi
