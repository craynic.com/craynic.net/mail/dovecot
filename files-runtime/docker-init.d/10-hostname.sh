#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$DOVECOT_HOSTNAME" ]]; then
  exit 0
fi

CFG_FILES=(/etc/dovecot*/dovecot.conf)

echo "Setting hostname to \"$DOVECOT_HOSTNAME\""

sed -i "/^#\?hostname[[:space:]]*=/c\\hostname = $DOVECOT_HOSTNAME" -- "${CFG_FILES[@]}"
