#!/usr/bin/env bash

set -Eeuo pipefail

CFG_FILES=(/etc/dovecot*/dovecot.conf)

if [[ "$DOVECOT_SUBMISSION_RELAY_SSL_VERIFY" =~ ^(no|false|0|off)$ ]]; then
  echo "!!! Allowing invalid TLS certificates for Submission relay !!!"

  echo "submission_relay_ssl_verify = no" | tee -a "${CFG_FILES[@]}" >/dev/null
fi
