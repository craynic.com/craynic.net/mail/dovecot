#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$DOVECOT_SUBMISSION_HOST" ]]; then
  exit 0
fi

if [[ ! "$DOVECOT_SUBMISSION_SSL" =~ ^yes|no$ ]]; then
  echo "Submission configuration missing or incorrect" > /dev/stderr
  exit 1
fi

CFG_FILES=(/etc/dovecot*/dovecot.conf)

# shellcheck disable=SC2140
echo "Setting submission host to \"$DOVECOT_SUBMISSION_HOST\""\
"$([[ $DOVECOT_SUBMISSION_SSL == "yes" ]] && echo " (encrypted connection)")"

sed -i "/^#\?submission_host[[:space:]]*=/c\\submission_host = $DOVECOT_SUBMISSION_HOST" -- "${CFG_FILES[@]}"
sed -i "/^#\?submission_ssl[[:space:]]*=/c\\submission_ssl = $DOVECOT_SUBMISSION_SSL" -- "${CFG_FILES[@]}"
