#!/usr/bin/env bash

set -Eeuo pipefail

CFG_FILES=(/etc/dovecot*/dovecot.conf)

if [[ -z "$DOVECOT_HAPROXY_TRUSTED_NETWORKS" ]]; then
  exit 0
fi

sed -i "/^#\?haproxy_trusted_networks[[:space:]]*=/c\\haproxy_trusted_networks = $DOVECOT_HAPROXY_TRUSTED_NETWORKS" \
  -- "${CFG_FILES[@]}"
