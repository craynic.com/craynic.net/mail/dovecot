#!/usr/bin/env bash

set -Eeuo pipefail

SRC_FOLDER="/etc/dovecot/virtual/"
DST_FOLDER="/var/mail/.virtual/"

rm -rf -- "$DST_FOLDER"
cp -R "$SRC_FOLDER" "$DST_FOLDER"

chown -R mail:mail -- "$DST_FOLDER"
chmod -R u+rX,u-w,go-rwx -- "$DST_FOLDER"
