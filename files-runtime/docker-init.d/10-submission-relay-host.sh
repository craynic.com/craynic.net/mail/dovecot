#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$DOVECOT_SUBMISSION_RELAY_HOST" || ! "$DOVECOT_SUBMISSION_RELAY_SSL" =~ ^no|smtps$|starttls ]]; then
  echo "Submission relay configuration missing or incorrect" > /dev/stderr
  exit 1
fi

CFG_FILES=(/etc/dovecot*/dovecot.conf)

# shellcheck disable=SC2140
echo "Setting submission relay host to \"$DOVECOT_SUBMISSION_RELAY_HOST\""\
"$([[ "$DOVECOT_SUBMISSION_RELAY_SSL" != "no" ]] && echo " with SSL type $DOVECOT_SUBMISSION_RELAY_SSL")"

if [[ -z "${DOVECOT_SUBMISSION_RELAY_HOST##*:*}" ]]; then
  HOST="${DOVECOT_SUBMISSION_RELAY_HOST%:*}"
  PORT="${DOVECOT_SUBMISSION_RELAY_HOST##*:}"
else
  HOST="$DOVECOT_SUBMISSION_RELAY_HOST"
  PORT="25"
fi

sed -i "/^#\?submission_relay_host[[:space:]]*=/c\\submission_relay_host = $HOST" -- "${CFG_FILES[@]}"
sed -i "/^#\?submission_relay_port[[:space:]]*=/c\\submission_relay_port = $PORT" -- "${CFG_FILES[@]}"
sed -i "/^#\?submission_relay_ssl[[:space:]]*=/c\\submission_relay_ssl = $DOVECOT_SUBMISSION_RELAY_SSL" \
  -- "${CFG_FILES[@]}"
