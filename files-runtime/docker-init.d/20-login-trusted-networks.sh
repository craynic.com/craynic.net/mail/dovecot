#!/usr/bin/env bash

set -Eeuo pipefail

CFG_FILES=(/etc/dovecot*/dovecot.conf)

if [[ -z "$DOVECOT_LOGIN_TRUSTED_NETWORKS" ]]; then
  exit 0
fi

for CFG_FILE in "${CFG_FILES[@]}"; do
  CURRENT_VALUE="$(grep "^login_trusted_networks" < "$CFG_FILE"| tail -n1)"
  sed -i "/^login_trusted_networks/c\\$CURRENT_VALUE $DOVECOT_LOGIN_TRUSTED_NETWORKS" -- "$CFG_FILE"
done
