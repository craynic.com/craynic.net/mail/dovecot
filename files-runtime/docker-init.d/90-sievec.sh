#!/usr/bin/env bash

set -Eeuo pipefail

find /usr/local/lib/dovecot/sieve/ -type d | while read -r DIR; do
  sievec "$DIR"
done

