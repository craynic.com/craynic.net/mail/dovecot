#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$DOVECOT_OAUTH2_API_HOST" || -z "$DOVECOT_OAUTH2_CLIENT_ID" || -z "$DOVECOT_OAUTH2_CLIENT_SECRET" ]]; then
    echo "OAuth2 credentials not set" >/dev/stderr
    exit 1
fi

TEMPLATE_FILES=(/etc/dovecot*/dovecot-oauth2.*.conf.ext.template)

if [[ ${#TEMPLATE_FILES[@]} -eq 0 ]]; then
  exit 0
fi

CFG_FILES=()

for TEMPLATE_FILE in "${TEMPLATE_FILES[@]}"; do
  CFG_FILE="${TEMPLATE_FILE%.template}"
  CFG_FILES+=("$CFG_FILE")

  # shellcheck disable=SC2016
  envsubst '${DOVECOT_OAUTH2_API_HOST} ${DOVECOT_OAUTH2_CLIENT_ID} ${DOVECOT_OAUTH2_CLIENT_SECRET}' \
    < "$TEMPLATE_FILE" > "$CFG_FILE"
done

# make sure the file has a newline at the end
echo "" | tee -a "${CFG_FILES[@]}" >/dev/null

if [[ -n "$DOVECOT_OAUTH2_SCOPE" ]]; then
  echo "Setting OAuth2 scope to \"$DOVECOT_OAUTH2_SCOPE\""
  echo "scope = $DOVECOT_OAUTH2_SCOPE" | tee -a "${CFG_FILES[@]}" >/dev/null
fi

if [[ "$DOVECOT_OAUTH2_DEBUGGING" =~ ^(yes|true|1|on)$ ]]; then
  echo "Enabling OAuth2 debugging"
  echo "debug = yes" | tee -a "${CFG_FILES[@]}" >/dev/null
fi

if [[ "$DOVECOT_OAUTH2_TLS_ALLOW_INVALID_CERT" =~ ^(yes|true|1|on)$ ]]; then
  echo "!!! Allowing invalid TLS certificates for OAuth2 !!!"
  echo "tls_allow_invalid_cert = yes" | tee -a "${CFG_FILES[@]}" >/dev/null

  sed -i "s/^\(pass_attrs.*starttls=\)yes/\1any-cert/" -- "${CFG_FILES[@]}"
fi

if compgen -G "/etc/dovecot/oauth2-keys/*" > /dev/null; then
  echo "Enabling local introspection mode"

  sed -i "/^#\?introspection_mode[[:space:]]*=/c\\introspection_mode = local" -- "${CFG_FILES[@]}"
  echo "local_validation_key_dict = fs:posix:prefix=/etc/dovecot/oauth2-keys/" | tee -a "${CFG_FILES[@]}" >/dev/null
fi
