#!/usr/bin/env bash

set -Eeuo pipefail

SCRIPTS=(/usr/local/lib/dovecot/sieve-extprograms/learn-*.sh)

if [[ -z "$DOVECOT_RSPAMD_HOST" ]]; then
  echo "Not enabling Rspamd learning"
  exit 0
fi

sed -i "/^RSPAM_HOST=\"\"/c\\RSPAM_HOST=\"$DOVECOT_RSPAMD_HOST\"" -- "${SCRIPTS[@]}"
sed -i "/^RSPAM_PASSWORD=\"\"/c\\RSPAM_PASSWORD=\"$DOVECOT_RSPAMD_PASSWORD\"" -- "${SCRIPTS[@]}"
