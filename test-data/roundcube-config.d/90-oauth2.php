<?php

$config['oauth_provider'] = 'generic';
$config['oauth_provider_name'] = 'Craynic';
$config['oauth_client_id'] = 'c610d694e47746faec0f0f9682734457';
$config['oauth_client_secret'] = '5d6a38f543ec230a9d2f788fa0a96710fd28b4396b20fc3b0cc44a4f472568cc'
    . 'cca3e14bdb6da1248e295fd2d43a261052b418f9f61449de487bcf5f37dfbceb';

$config['oauth_auth_uri'] = 'https://localhost:8443/oauth2/authorize';
$config['oauth_token_uri'] = 'https://host.docker.internal:8443/api/oauth2/token';
$config['oauth_identity_uri'] = 'https://host.docker.internal:8443/api/oauth2/introspection';
$config['oauth_scope'] = 'email';

$config['oauth_auth_parameters'] = [];
$config['oauth_identity_fields'] = ['username'];
$config['oauth_login_redirect'] = true;

$config['oauth_verify_peer'] = false;
