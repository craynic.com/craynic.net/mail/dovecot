FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

ENV DOVECOT_AUTH_DEBUG="no" \
    DOVECOT_AUTH_POLICY_ENDPOINT="" \
    DOVECOT_AUTH_POLICY_HASH_NONCE="" \
    DOVECOT_AUTH_VERBOSE="no" \
    DOVECOT_HAPROXY_TRUSTED_NETWORKS="" \
    DOVECOT_HOSTNAME="" \
    DOVECOT_LOGIN_TRUSTED_NETWORKS="" \
    DOVECOT_MYSQL_CA="" \
    DOVECOT_MYSQL_DSN="" \
    DOVECOT_OAUTH2_API_HOST="" \
    DOVECOT_OAUTH2_CLIENT_ID="" \
    DOVECOT_OAUTH2_CLIENT_SECRET="" \
    DOVECOT_OAUTH2_DEBUGGING="no" \
    DOVECOT_OAUTH2_SCOPE="" \
    DOVECOT_OAUTH2_TLS_ALLOW_INVALID_CERT="no" \
    DOVECOT_RSPAMD_HOST="" \
    DOVECOT_RSPAMD_PASSWORD="" \
    DOVECOT_SSL_CLIENT_REQUIRE_VALID_CERT="yes" \
    DOVECOT_STORAGE_FIX_PERMISSIONS_ON_STARTUP="yes" \
    DOVECOT_STORAGE_FIX_PERMISSIONS_ON_STARTUP_RECURSIVE="no" \
    DOVECOT_SUBMISSION_HOST="" \
    DOVECOT_SUBMISSION_MAX_MAIL_SIZE="" \
    DOVECOT_SUBMISSION_RELAY_HOST="" \
    DOVECOT_SUBMISSION_RELAY_SSL="no" \
    DOVECOT_SUBMISSION_RELAY_SSL_VERIFY="yes" \
    DOVECOT_SUBMISSION_SSL="no"

# renovate: datasource=repology depName=alpine_3_21/dovecot depType=dependencies versioning=loose
ARG DOVECOT_VERSION="2.3.21.1-r0"
# renovate: datasource=repology depName=alpine_3_21/dovecot-fts-flatcurve depType=dependencies versioning=loose
ARG DOVECOT_FTS_FLATCURVE_VERSION="1.0.5-r0"
# renovate: datasource=repology depName=alpine_3_21/rspamd-client depType=dependencies versioning=loose
ARG RSPAMD_CLIENT_VERSION="3.10.2-r2"

RUN apk add --no-cache \
        supervisor~=4 \
        dovecot="${DOVECOT_VERSION}" \
        dovecot-fts-flatcurve="${DOVECOT_FTS_FLATCURVE_VERSION}" \
        dovecot-lmtpd="${DOVECOT_VERSION}" \
        dovecot-mysql="${DOVECOT_VERSION}" \
        dovecot-pigeonhole-plugin="${DOVECOT_VERSION}" \
        dovecot-pop3d="${DOVECOT_VERSION}" \
        dovecot-submissiond="${DOVECOT_VERSION}" \
        rspamd-client="${RSPAMD_CLIENT_VERSION}" \
        iproute2~=6 \
        bash~=5 \
        run-parts~=4 \
        gettext~=0 \
        mysql-client~=11 \
        moreutils~=0 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3 \
    && mkdir -p /run/dovecot/sieve-pipe/

COPY files-runtime/ /

EXPOSE 24 110 143 465 587 993 995 4190 10110 10143 10465 10587 10993 10995 14190

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/probe.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]

CMD ["/usr/local/sbin/supervisord.sh"]
