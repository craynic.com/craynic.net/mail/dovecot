# Dovecot server
This container provides Dovecot IMAP/POP3 mail server with XOAuth2 support. It's part of
a suite of containers that together form a complete mail server solution. For more check
https://gitlab.com/craynic.com/craynic.net/mail/.

## Container ports

|Port|Service|HAProxy?|Encryption type|
|----|-------|--------|---------------|
|24|LMTP|no|STARTTLS|
|110|POP3|no|STARTTLS|
|995|POP3|no|SSL|
|10110|POP3|yes|STARTTLS|
|10995|POP3|yes|SSL|
|143|IMAP|no|STARTTLS|
|993|IMAP|no|SSL|
|10143|IMAP|yes|STARTTLS|
|10993|IMAP|yes|SSL|
|465|SMTPS|no|SSL|
|10465|SMTPS|yes|SSL|
|587|Submission|no|STARTTLS|
|10587|Submission|yes|STARTTLS|
|4190|Managesieve|no|STARTTLS|
|14190|Managesieve|yes|STARTTLS|

## Mountpoints

The container expects the following mountspoints to be used to provide files necessary to run: 

* **/var/mail/**
  * A directory holding the actual mailboxes, including search indexes and sieve scripts
  * Should be initially empty
  * Dovecot must have permissions to write into the folder
* **/etc/ssl/dovecot/**
  * Needs to contain 2 files, `server.pem` and `server.key`, holding the SSL certificate and key
  * Should be mounted as read-only
* **/etc/ssl/dovecot-dh/**
  * Needs to contain 1 files, `dh.pem`, holding the Diffie-Hellman parameters
  * Should be mounted as read-only

Additionally to that, these mount point can be used to enable optional features:

* **/etc/dovecot/oauth2-keys/**
  * Contains keys for local validation of OAuth2 JWT tokens
  * Local validation is enabled only when this directory is not empty
  * Should be mounted as read-only
  * For more information see https://doc.dovecot.org/configuration_manual/authentication/oauth2/
